PyCalc
=========================
***
Author : Igor ENEPUNIXOID Linux

Date: Декабрь 2021

Site: [enep-home.ru](https://enep-home.ru)

***


Calculator written in the Python programming language and the pySide2 library

## Dependencies
Python library [pySide2](https://pypi.org/project/PySide2/) 
```bash
pip install pyside2 
```
you can also install using the OS package manager.

***
@2022 [ENEP-HOME.ru](https://enep-home.ru), Igor ENEPUNIXOID Kolonchenko 