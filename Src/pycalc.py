# -*- coding: utf-8 -*-
import sys
import math
from PySide2.QtWidgets import *

from pc_form import Ui_MainWindow
import pc_rc

class Calc(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setFixedSize(460,416)
        self.btn0.clicked.connect(self.evDigitPress)
        self.btn1.clicked.connect(self.evDigitPress)
        self.btn2.clicked.connect(self.evDigitPress)
        self.btn3.clicked.connect(self.evDigitPress)
        self.btn4.clicked.connect(self.evDigitPress)
        self.btn5.clicked.connect(self.evDigitPress)
        self.btn6.clicked.connect(self.evDigitPress)
        self.btn7.clicked.connect(self.evDigitPress)
        self.btn8.clicked.connect(self.evDigitPress)
        self.btn9.clicked.connect(self.evDigitPress)


    def evDigitPress(self):
        bt = self.sender()
        if self.lineEdit.text() == '0':
            self.lineEdit.setText(bt.text())
        else:
            if self.result == self.lineEdit.text():
                self.lineEdit.setText(bt.text())
            else:
                self.lineEdit.setText(self.lineEdit.text() + bt.text())
        self.result = 0



if __name__ == "__main__":
    app = QApplication(sys.argv)
    calc = Calc()
    calc.show()
    sys.exit(app.exec_())





