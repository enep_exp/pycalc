# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'pc_formmMrKHK.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import pc_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(460, 416)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setLayoutDirection(Qt.LeftToRight)
        self.centralwidget.setStyleSheet(u"QWidget {\n"
"	background-color:#121212;\n"
"	color:#fff;\n"
"	font: 16pt \"PROFONT\";\n"
"}\n"
"QPushButton {\n"
"	border:none;\n"
"	\n"
"}\n"
"QPushButton:hover {\n"
"	background-color:#666;\n"
"}\n"
"QLideEdit {\n"
"	font-size: 40pt\n"
"    color:#fff;\n"
"}")
        self.lineEdit = QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(10, 10, 441, 31))
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit.sizePolicy().hasHeightForWidth())
        self.lineEdit.setSizePolicy(sizePolicy)
        self.lineEdit.setLayoutDirection(Qt.RightToLeft)
        self.lineEdit.setMaxLength(10)
        self.lineEdit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.lineEdit.setReadOnly(True)
        self.gridLayoutWidget = QWidget(self.centralwidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(8, 50, 441, 361))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.btn4 = QPushButton(self.gridLayoutWidget)
        self.btn4.setObjectName(u"btn4")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btn4.sizePolicy().hasHeightForWidth())
        self.btn4.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn4, 2, 0, 1, 1)

        self.pushButton_2 = QPushButton(self.gridLayoutWidget)
        self.pushButton_2.setObjectName(u"pushButton_2")
        sizePolicy1.setHeightForWidth(self.pushButton_2.sizePolicy().hasHeightForWidth())
        self.pushButton_2.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_2, 0, 3, 1, 1)

        self.pushButton_4 = QPushButton(self.gridLayoutWidget)
        self.pushButton_4.setObjectName(u"pushButton_4")
        sizePolicy1.setHeightForWidth(self.pushButton_4.sizePolicy().hasHeightForWidth())
        self.pushButton_4.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_4, 0, 1, 1, 1)

        self.pushButton_5 = QPushButton(self.gridLayoutWidget)
        self.pushButton_5.setObjectName(u"pushButton_5")
        sizePolicy1.setHeightForWidth(self.pushButton_5.sizePolicy().hasHeightForWidth())
        self.pushButton_5.setSizePolicy(sizePolicy1)
        icon = QIcon()
        icon.addFile(u":/icons/media/outline_backspace_white_48dp.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_5.setIcon(icon)

        self.gridLayout.addWidget(self.pushButton_5, 0, 0, 1, 1)

        self.btn7 = QPushButton(self.gridLayoutWidget)
        self.btn7.setObjectName(u"btn7")
        sizePolicy1.setHeightForWidth(self.btn7.sizePolicy().hasHeightForWidth())
        self.btn7.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn7, 1, 0, 1, 1)

        self.btn8 = QPushButton(self.gridLayoutWidget)
        self.btn8.setObjectName(u"btn8")
        sizePolicy1.setHeightForWidth(self.btn8.sizePolicy().hasHeightForWidth())
        self.btn8.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn8, 1, 1, 1, 1)

        self.pushButton_10 = QPushButton(self.gridLayoutWidget)
        self.pushButton_10.setObjectName(u"pushButton_10")
        sizePolicy1.setHeightForWidth(self.pushButton_10.sizePolicy().hasHeightForWidth())
        self.pushButton_10.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_10, 1, 4, 1, 1)

        self.pushButton_9 = QPushButton(self.gridLayoutWidget)
        self.pushButton_9.setObjectName(u"pushButton_9")
        sizePolicy1.setHeightForWidth(self.pushButton_9.sizePolicy().hasHeightForWidth())
        self.pushButton_9.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_9, 1, 3, 1, 1)

        self.btn9 = QPushButton(self.gridLayoutWidget)
        self.btn9.setObjectName(u"btn9")
        sizePolicy1.setHeightForWidth(self.btn9.sizePolicy().hasHeightForWidth())
        self.btn9.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn9, 1, 2, 1, 1)

        self.btn1 = QPushButton(self.gridLayoutWidget)
        self.btn1.setObjectName(u"btn1")
        sizePolicy1.setHeightForWidth(self.btn1.sizePolicy().hasHeightForWidth())
        self.btn1.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn1, 3, 0, 1, 1)

        self.pushButton = QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName(u"pushButton")
        sizePolicy1.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton, 0, 4, 1, 1)

        self.pushButton_3 = QPushButton(self.gridLayoutWidget)
        self.pushButton_3.setObjectName(u"pushButton_3")
        sizePolicy1.setHeightForWidth(self.pushButton_3.sizePolicy().hasHeightForWidth())
        self.pushButton_3.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_3, 0, 2, 1, 1)

        self.btn5 = QPushButton(self.gridLayoutWidget)
        self.btn5.setObjectName(u"btn5")
        sizePolicy1.setHeightForWidth(self.btn5.sizePolicy().hasHeightForWidth())
        self.btn5.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn5, 2, 1, 1, 1)

        self.btn6 = QPushButton(self.gridLayoutWidget)
        self.btn6.setObjectName(u"btn6")
        sizePolicy1.setHeightForWidth(self.btn6.sizePolicy().hasHeightForWidth())
        self.btn6.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn6, 2, 2, 1, 1)

        self.btn2 = QPushButton(self.gridLayoutWidget)
        self.btn2.setObjectName(u"btn2")
        sizePolicy1.setHeightForWidth(self.btn2.sizePolicy().hasHeightForWidth())
        self.btn2.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn2, 3, 1, 1, 1)

        self.btn0 = QPushButton(self.gridLayoutWidget)
        self.btn0.setObjectName(u"btn0")
        sizePolicy1.setHeightForWidth(self.btn0.sizePolicy().hasHeightForWidth())
        self.btn0.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn0, 4, 1, 1, 1)

        self.btn3 = QPushButton(self.gridLayoutWidget)
        self.btn3.setObjectName(u"btn3")
        sizePolicy1.setHeightForWidth(self.btn3.sizePolicy().hasHeightForWidth())
        self.btn3.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.btn3, 3, 2, 1, 1)

        self.pushButton_19 = QPushButton(self.gridLayoutWidget)
        self.pushButton_19.setObjectName(u"pushButton_19")
        sizePolicy1.setHeightForWidth(self.pushButton_19.sizePolicy().hasHeightForWidth())
        self.pushButton_19.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_19, 4, 2, 1, 1)

        self.pushButton_20 = QPushButton(self.gridLayoutWidget)
        self.pushButton_20.setObjectName(u"pushButton_20")
        sizePolicy1.setHeightForWidth(self.pushButton_20.sizePolicy().hasHeightForWidth())
        self.pushButton_20.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_20, 2, 3, 1, 1)

        self.pushButton_21 = QPushButton(self.gridLayoutWidget)
        self.pushButton_21.setObjectName(u"pushButton_21")
        sizePolicy1.setHeightForWidth(self.pushButton_21.sizePolicy().hasHeightForWidth())
        self.pushButton_21.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_21, 3, 3, 1, 1)

        self.pushButton_22 = QPushButton(self.gridLayoutWidget)
        self.pushButton_22.setObjectName(u"pushButton_22")
        sizePolicy1.setHeightForWidth(self.pushButton_22.sizePolicy().hasHeightForWidth())
        self.pushButton_22.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_22, 4, 3, 1, 1)

        self.pushButton_23 = QPushButton(self.gridLayoutWidget)
        self.pushButton_23.setObjectName(u"pushButton_23")
        sizePolicy1.setHeightForWidth(self.pushButton_23.sizePolicy().hasHeightForWidth())
        self.pushButton_23.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_23, 2, 4, 1, 1)

        self.pushButton_24 = QPushButton(self.gridLayoutWidget)
        self.pushButton_24.setObjectName(u"pushButton_24")
        sizePolicy1.setHeightForWidth(self.pushButton_24.sizePolicy().hasHeightForWidth())
        self.pushButton_24.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_24, 3, 4, 1, 1)

        self.pushButton_25 = QPushButton(self.gridLayoutWidget)
        self.pushButton_25.setObjectName(u"pushButton_25")
        sizePolicy1.setHeightForWidth(self.pushButton_25.sizePolicy().hasHeightForWidth())
        self.pushButton_25.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.pushButton_25, 4, 4, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.lineEdit.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("MainWindow", u"0", None))
        self.btn4.setText(QCoreApplication.translate("MainWindow", u"4", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"+/-", None))
        self.pushButton_4.setText(QCoreApplication.translate("MainWindow", u"CE", None))
        self.pushButton_5.setText("")
        self.btn7.setText(QCoreApplication.translate("MainWindow", u"7", None))
        self.btn8.setText(QCoreApplication.translate("MainWindow", u"8", None))
        self.pushButton_10.setText(QCoreApplication.translate("MainWindow", u"%", None))
        self.pushButton_9.setText(QCoreApplication.translate("MainWindow", u"/", None))
        self.btn9.setText(QCoreApplication.translate("MainWindow", u"9", None))
        self.btn1.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"sqrt", None))
        self.pushButton_3.setText(QCoreApplication.translate("MainWindow", u"C", None))
        self.btn5.setText(QCoreApplication.translate("MainWindow", u"5", None))
        self.btn6.setText(QCoreApplication.translate("MainWindow", u"6", None))
        self.btn2.setText(QCoreApplication.translate("MainWindow", u"2", None))
        self.btn0.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.btn3.setText(QCoreApplication.translate("MainWindow", u"3", None))
        self.pushButton_19.setText(QCoreApplication.translate("MainWindow", u".", None))
        self.pushButton_20.setText(QCoreApplication.translate("MainWindow", u"*", None))
        self.pushButton_21.setText(QCoreApplication.translate("MainWindow", u"+", None))
        self.pushButton_22.setText(QCoreApplication.translate("MainWindow", u"-", None))
        self.pushButton_23.setText(QCoreApplication.translate("MainWindow", u"1/X", None))
        self.pushButton_24.setText(QCoreApplication.translate("MainWindow", u"=", None))
        self.pushButton_25.setText(QCoreApplication.translate("MainWindow", u"EXIT", None))
    # retranslateUi

